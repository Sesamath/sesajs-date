# sesajs-date

Le dépôt de code a déménagé et se trouve désormais sur
https://git.sesamath.net/sesamath/sesajs-date

Vous pouvez indiquez dans vos dépendances (package.json)
```
  "sesajs-date": "git+https://git.sesamath.net/sesamath/sesajs-date.git",
```
